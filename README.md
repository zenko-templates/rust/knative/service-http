# Knative Rust HTTP Service Starter

A starter to generate Knative http service using rust and warp

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Deploy](#deploy)

## Installation

Clone the project

```sh
git clone git@gitlab.com:zenko-templates/rust/knative/http-service.git
```

Update the files:
- [ ] The name in the [k8s service file](k8s/service.yaml)

Remove the boilerplate exemple: (Optional)
- [ ] The file [todo_created.rs](src/messages/todo_created.rs)
- [ ] The structure body inside [dto.rs](src/dto.rs) 
- [ ] The logic inside [handlers.rs](src/handlers.rs) 

Create a new repository

```sh
rm -rf .git
git init
git add .
git commit -m "initial commit"
git remote add origin git@gitlab.com:
git push --all
```

## Usage

To handle a incoming request you only need to modify four files:
- [ ] [handlers.rs](src/handlers.rs), it contains all the logic
- [ ] [filters.rs](src/filters.rs), you can change the HTTP verb in the main function
- [ ] [dto.rs](src/dto.rs), it contains the structure of incoming dto body
- [ ] Create one or multiple files in [message folder](src/messages/) to represent the data of a event

## Deploy

Update the config file:
- [ ] The environment variable in the [k8s service file](k8s/service.yaml)

```sh
kube apply -f k8s/service.yaml
```

