use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Body {
    pub title: String,
    pub description: Option<String>,
}
