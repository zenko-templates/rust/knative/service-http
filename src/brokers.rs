pub mod http;

use async_trait::async_trait;
use cloudevents::Event;
use std::fmt::Debug;

use crate::config::Config;

#[async_trait]
pub trait Broker {
    type Error: Debug;

    fn new(config: Config) -> Self;
    async fn send(self, event: Event) -> Result<(), Self::Error>;
}
