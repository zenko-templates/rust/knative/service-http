use cloudevents::event::EventBuilderError;

#[derive(Debug)]
pub enum Error {
    Serialization,
    CloudEvent(EventBuilderError),
}
