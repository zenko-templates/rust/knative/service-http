use std::convert::Infallible;
use uuid::Uuid;
use warp::hyper::StatusCode;

use crate::{
    brokers::Broker, config::Config, dto::Body, events::Event, messages::todo_created::TodoCreated,
};

pub async fn handle(
    _config: Config,
    broker: impl Broker,
    body: Body,
) -> Result<impl warp::Reply, Infallible> {
    log::debug!(target: "handler", "Handle request");
    log::debug!(target: "handler", "\t-> Body: {:?}", body);

    // TODO: Create the message here
    let mut message = TodoCreated::default();
    message.set_id(Uuid::new_v4().to_string());
    message.set_version(1);
    message.set_title(body.title);
    if let Some(description) = body.description {
        message.set_description(description);
    }

    log::debug!(target: "handler", "\t-> Message: {:?}", message);

    let event = match Event::new(message).build() {
        Ok(event) => event,
        Err(error) => {
            log::error!(target: "handler", "\t-> Error when creating the event: {:?}", error);
            return Ok(StatusCode::INTERNAL_SERVER_ERROR);
        }
    };

    log::debug!(target: "handler", "\t-> Event: {:?}", event);

    let res = broker.send(event).await;

    match res {
        Ok(_) => Ok(StatusCode::NO_CONTENT),
        Err(error) => {
            log::debug!(target: "handler", "\t->Error when sending the event: {:?}", error);
            return Ok(StatusCode::INTERNAL_SERVER_ERROR);
        }
    }
}
