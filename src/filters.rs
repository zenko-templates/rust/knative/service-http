use serde::de::DeserializeOwned;
use warp::Filter;

use crate::{
    brokers::{http::HttpBroker, Broker},
    config::{broker::BrokerType, Config},
    handlers,
};

pub fn main(
    config: Config,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::post()
        .and(with_config(config.clone()))
        .and(with_broker(config.clone()))
        .and(json_body())
        .and_then(handlers::handle)
}

fn with_config(
    config: Config,
) -> impl Filter<Extract = (Config,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || config.clone())
}

fn with_broker(
    config: Config,
) -> impl Filter<Extract = (impl Broker,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || match config.broker.ty {
        BrokerType::HTTP => HttpBroker::new(config.clone()),
    })
}

fn json_body<T: DeserializeOwned + Send>(
) -> impl Filter<Extract = (T,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}
