pub mod todo_created;

use serde::Serialize;

pub trait Message: Serialize + Clone {
    const TYPE: &'static str;

    fn id(self) -> String;
}
