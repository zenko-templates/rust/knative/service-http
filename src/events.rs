mod error;

use cloudevents::{EventBuilder, EventBuilderV10};
use uuid::Uuid;

use crate::messages::Message;

use self::error::Error;

pub struct Event<T: Message> {
    message: T,
}

impl<T: Message> Event<T> {
    pub fn new(data: T) -> Self {
        Event { message: data }
    }

    pub fn build(&self) -> Result<cloudevents::Event, Error> {
        log::debug!(target: "event", "Build event");

        let value = match serde_json::to_value(self.message.clone()) {
            Ok(value) => value,
            Err(error) => {
                log::error!(target: "event", "\t-> Error when serializing the message: {:?}", error);
                return Err(Error::Serialization);
            }
        };
        log::debug!(target: "event", "\t-> Value: {:?}", value);

        match EventBuilderV10::new()
            .id(Uuid::new_v4().to_string())
            .ty(T::TYPE)
            .source("com.zenko-fox")
            .extension("partitionkey", self.message.clone().id())
            .data("application/json", value)
            .build()
        {
            Ok(event) => Ok(event),
            Err(error) => {
                log::error!(target: "event", "\t-> Error when creating the event: {:?}", error);
                Err(Error::CloudEvent(error))
            }
        }
    }
}
