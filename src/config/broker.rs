use envconfig::Envconfig;
use std::str::FromStr;

#[derive(Clone)]
pub enum BrokerType {
    HTTP,
}

impl FromStr for BrokerType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.trim().to_lowercase().as_ref() {
            "http" => Ok(BrokerType::HTTP),
            _ => Err(format!("Unknown Broker type: {s}")),
        }
    }
}

#[derive(Envconfig, Clone)]
pub struct Broker {
    #[envconfig(from = "BROKER_TYPE")]
    pub ty: BrokerType,

    #[envconfig(from = "BROKER_URI")]
    pub uri: Option<String>,
}
