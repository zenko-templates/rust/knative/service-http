use serde::Serialize;

use super::Message;

#[derive(Debug, Default, Serialize, Clone)]
pub struct TodoCreated {
    id: String,
    title: String,
    description: Option<String>,
    version: u8,
}

impl TodoCreated {
    pub fn set_id(&mut self, id: String) {
        self.id = id
    }

    pub fn set_version(&mut self, version: u8) {
        self.version = version
    }

    pub fn set_title(&mut self, title: String) {
        self.title = title
    }

    pub fn set_description(&mut self, description: String) {
        self.description = Some(description)
    }
}

impl Message for TodoCreated {
    const TYPE: &'static str = "todo.created";

    fn id(self) -> String {
        self.id
    }
}
